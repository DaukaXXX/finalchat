drop table if exists users;
create table users
(
    id serial,
    name varchar(255)
);

drop table if exists chat;
create table chat
(
    id serial,
    name varchar(255)
);

drop table if exists message;
create table message(
    id serial,
    user_id bigint,
    chat_id bigint,
    text varchar(255)
);
alter table message
add column message_type varchar(255);

drop table if exists auth;
create table auth(
login varchar(255),
password varchar(255),
last_login_timestamp varchar(255),
user_id varchar(255),
token varchar(255)
);