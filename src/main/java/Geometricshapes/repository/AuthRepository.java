package Geometricshapes.repository;

import Geometricshapes.model.auth;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AuthRepository extends JpaRepository<auth, String> {
    default auth getToken(UUID token) {

        return getToken(token);
    }
    auth getByloginandpassword(String login, String password);


}





