package Geometricshapes.repository;

import Geometricshapes.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findAllByIsReadFalseAndChatIdAndUserId(Long chatId, Long userId);

    List<Message> findAllByIsDeliveredFalseAndUserId(Long userId);

    List<Message> findAllByChatIdAndUserId(Long chatId, Long userId);

    List<Message> findAllByChatIdAndIsDeliveredFalse(Long chatId);

}


