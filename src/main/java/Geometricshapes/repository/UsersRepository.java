package Geometricshapes.repository;

import Geometricshapes.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsersRepository extends JpaRepository<Users, Long> {
    List<Users> findAllByUserId(Long userId);
    List<Users> findAllByChatId(Long chatId);


}
