package Geometricshapes.service;

import Geometricshapes.model.Message;
import Geometricshapes.model.MessageCG;
import Geometricshapes.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
    public class MessageService {
        private MessageRepository messageRepository;

        public List<Message> findAll() {
            List<Message> messages= messageRepository.findAll();
            return messageRepository.findAll();
        }

        public MessageCG findById(Long id) {
            Optional<Message> message = messageRepository.findById(id);
        }

    public Message save(Message message) {
        if (message.getMessageType()==("basic")){
            message.setMessageType(message.getText());
        }
        else if (message.getMessageType()==("online")){
            message.setMessageType("user in chat");
        }
        else if (message.getMessageType()==("writing")){
            message.setMessageType("user starts write message");
        }

        return messageRepository.save(message);
    }

    public void deleteById(Long id) {

            messageRepository.deleteById(id);
        }
    }


