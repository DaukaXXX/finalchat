package Geometricshapes.service;

import Geometricshapes.model.Chat;
import Geometricshapes.model.Users;
import Geometricshapes.repository.ChatRepository;
import Geometricshapes.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class ChatService {
    private ChatRepository chatRepository;

    private UsersRepository usersRepository;

    public List<Users> getUsersByChatId(Long chat_id) {
        List<Users> users = new ArrayList<>();
        if (chatRepository.existsById(chat_id)) {
            List<Users> participants = UsersRepository.findAllByChatId(chat_id);
            for (Users user : users) {
                users.add(usersRepository.findById(user.getUserId()).get());
            }
        }
        return users;
    }


    public List<Chat> findAllChatsByUserId(Long userId) {
        List<Chat> chatList = new ArrayList<>();
        List<User> users = UsersRepository.findAllByUserId(userId);

        for (Users user : users) {
            chatList.add(chatRepository.findById(user.getChatId()).get());
        }
        return chatList;
    }

    public List<Chat> findAll(){

        return chatRepository.findAll();
    }

    public void deleteById(Long id){

        chatRepository.deleteById(id);
    }

    public Chat save(Chat message){

        return chatRepository.save(message);
    }

    public Chat findById(Long id){

        return chatRepository.findById(id).get();
    }

}
