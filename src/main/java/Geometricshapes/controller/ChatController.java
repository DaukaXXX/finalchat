package Geometricshapes.controller;

import Geometricshapes.model.Chat;
import Geometricshapes.model.auth;
import Geometricshapes.repository.UsersRepository;
import Geometricshapes.service.AuthService;
import Geometricshapes.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/v1/chats")
@AllArgsConstructor
public class ChatController {
    private ChatService chatService;

    private UsersRepository usersRepository;

    @GetMapping("")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(chatService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){

        return ResponseEntity.ok(chatService.findById(id));
    }

    @PostMapping("")
    public ResponseEntity<?> addChat(@RequestBody Chat chat){

        return ResponseEntity.ok(chatService.save(chat));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteChat(@PathVariable Long id){
       chatService.deleteById(id);
       return ResponseEntity.noContent().build();
    }
    @GetMapping("/find/{userId}")
    public ResponseEntity<?> findAllChatsByUserId(@PathVariable Long userId){
        return ResponseEntity.ok(chatService.findAllChatsByUserId(userId));
    }

    @GetMapping("/findUsers/{chatId}")
    public ResponseEntity<?> findAllByChatId(@PathVariable Long chatId, @RequestHeader(name = "token")Long token) {
        auth auth= AuthService.getToken(token);
        return ResponseEntity.ok(chatService.getUsersByChatId(chatId,auth.getUser_id()));
    }

}
